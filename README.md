# Tarte au citron

Comply to the European cookie law using [tarteaucitron.js](https://github.com/AmauriC/tarteaucitron.js).

## Installation

1. Download tarteaucitron library from [tarteaucitron.js](https://github.com/AmauriC/tarteaucitron.js) and place it into /libraries/tarteaucitron
2. Download the module and enable it
3. Enable needed submodules
4. Configure at Administer > Configuration >
Tarte au citron > Settings for Tarte au citron module.


### Composer installation
#### Composer without composer merge plugin
1. Add the tarteaucitron.js repository in your project composer.json

```json
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "amauric/tarteaucitron",
            "type": "drupal-library",
            "version": "1.17.0",
            "require": {
                "composer/installers": "*"
            },
            "dist": {
              "type": "zip",
              "url": "https://github.com/AmauriC/tarteaucitron.js/archive/refs/tags/v1.17.0.zip"
            }
        }
    }
],
```

2. Add the module and library
```bash
composer require amauric/tarteaucitron
composer require drupal/tarte_au_citron
```

#### Composer with composer merge plugin
1. Install composer merge plugin if it's not already installed
```bash
composer wikimedia/composer-merge-plugin
```

2. Edit the "composer.json" file of your website and under the "extra" section add lines as following:
\* note: the `web` represents the folder where drupal lives like: ex. `docroot`.

```json
"extra": {
  "merge-plugin": {
    "include": [
      "web/modules/contrib/tarte_au_citron/composer.libraries.json"
    ]
  }
},
```

2. Add the module
```bash
composer require drupal/tarte_au_citron
```

## Features

- Configure javascript library
- Add services
- Edit texts
